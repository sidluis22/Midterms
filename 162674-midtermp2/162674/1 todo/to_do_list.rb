class TodoList

@qtask = []
@wtask = []

def add_task(task_name)
	@wtask << task_name
	puts "#{task_name} has been added to your task list"
end

def view_pending_tasks
	puts "Pending tasks:"

	@wtask.each_with_index do |task, array_position|
		puts "(#{x}). #{task}"
		x = array_position + 1
	end
end

def accomplish_task(index)
	@qtask << @wtask[index]

	@wtask.delete_at(index)
end

def view_accomplished_tasks
	puts "Accomplished tasks:"

	@qtask.each_with_index do |task, array_position|
		puts "(#{x}). #{task}"
		x = array_position + 1
	end
end

loop do 
	puts "1. Add a task"
	puts "2. View pending tasks"
	puts "3. Accomplish task"
	puts "4. View Accomplished tasks"
	puts "0. Exit"
	print "Select an action: "

	first_choice = gets.chomp

	if first_choice = 1
		print "Input task: "
		taskname = gets.chomp
		add_task(taskname)
	end

	if first_choice = 2
		view_pending_tasks
	end

	if first_choice = 3
		view_pending_tasks

		puts "Which task to do?"
		x = gets.chomp
		x =- 1
		accomplish_task(x)
	end

	if first_choice = 4
		view_accomplished_tasks
	end

	break if first_choice == 0
end

end